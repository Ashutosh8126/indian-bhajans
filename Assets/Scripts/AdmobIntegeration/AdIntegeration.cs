﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;

public class AdIntegeration : MonoBehaviour {

	public string adUnitID;

	// Use this for initialization
	void Start () {	
		adUnitID = "ca-app-pub-8092078157357126/2941205693";
		Invoke ("DisplayBanner",2);
	}
	
	internal void DisplayBanner(){		
		Debug.Log ("Displaying Banner");
		// Create a 320x50 banner at the top of the screen.
		BannerView bannerView = new BannerView(adUnitID, AdSize.Banner, AdPosition.Bottom);
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder().Build();
		// Load the banner with the request.
		bannerView.LoadAd(request);
	}
}
