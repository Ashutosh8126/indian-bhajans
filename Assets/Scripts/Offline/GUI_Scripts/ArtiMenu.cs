﻿using UnityEngine;
using System.Collections;

public class ArtiMenu : MonoBehaviour {

#region public_Variables

#endregion

#region private_Variables
	[SerializeField]private MainMenu_Panel mainMenuPanelScript;

	private Transform weeklyPrayerPanel;
    private Transform godPrayerPanel;
	private Transform goddessPrayerPanel;
    private PrayerAudio prayerAudioScript;
    private FadePanel fadePanelScript;
//	private Transform 
#endregion


	// Use this for initialization
	void Awake () {
//		mainMenuPanelScript = this.transform.parent.transform.Find ("MainMenu_Panel").GetComponent<MainMenu_Panel> ();
		prayerAudioScript = Camera.main.GetComponent<PrayerAudio>();
		weeklyPrayerPanel = this.transform.parent.transform.Find("PrayerDisplay_Panel/Display_Panel/Weekly_Artis").transform;
        godPrayerPanel = this.transform.parent.transform.Find("PrayerDisplay_Panel/Display_Panel/God_Artis").transform;
		goddessPrayerPanel = this.transform.parent.transform.Find("PrayerDisplay_Panel/Display_Panel/Goddess_Artis").transform;
        fadePanelScript = this.transform.parent.transform.Find("FadePanel").GetComponent<FadePanel>();
    }

    void OnDisable() {
        weeklyPrayerPanel.gameObject.SetActive(false);
        goddessPrayerPanel.gameObject.SetActive(false);
        godPrayerPanel.gameObject.SetActive(false);
    }

	internal void SetArti_Menu(string menuName){
		this.gameObject.SetActive (true);
		foreach(Transform child in this.transform){
			if (child.name.Equals (menuName)) {
				Debug.Log ("Menu Found " + menuName);
				child.gameObject.SetActive (true);
			} else if (child.name.Equals ("BG")) {
				child.gameObject.SetActive (true);
			} else {
				child.gameObject.SetActive (false);
			}
		}
	}

	public void OnWeeklyDayButtonPressed(string weekDay){
		Debug.Log ("Weekly Button Pressed "+weekDay);
		this.gameObject.SetActive (false);
		weeklyPrayerPanel.transform.parent.transform.parent.gameObject.SetActive (true);
		weeklyPrayerPanel.transform.gameObject.SetActive (true);
        fadePanelScript.BlackFade();
		foreach(Transform child in weeklyPrayerPanel.transform){
			if (child.name.Equals (weekDay)) {
				Debug.Log ("Attaching "+weekDay);
				child.gameObject.SetActive (true);				
                prayerAudioScript.AssignPrayer("Audio/Weekly/Aarti_"+child.name);               
            } else {
				child.gameObject.SetActive (false);
			}
		}
	}

	public void OnGoddessButtonPressed(string goddessName){
		this.gameObject.SetActive(false);
		goddessPrayerPanel.transform.parent.transform.parent.gameObject.SetActive(true);
		goddessPrayerPanel.gameObject.SetActive (true);
		Debug.Log("Activate "+goddessName+ "prayer");
        fadePanelScript.BlackFade();
        foreach (Transform child in goddessPrayerPanel.transform)
		{
			if (child.name.Equals(goddessName))
			{
				child.gameObject.SetActive(true);               
				prayerAudioScript.AssignPrayer("Audio/Goddess/"+child.name);
			}
			else {
				child.gameObject.SetActive(false);
			}
		}
	}

    public void OnGodButtonPressed(string godName) {
        this.gameObject.SetActive(false);
        godPrayerPanel.transform.parent.transform.parent.gameObject.SetActive(true);
		godPrayerPanel.gameObject.SetActive (true);
        Debug.Log("Activate "+godName+ "prayer");
        fadePanelScript.BlackFade();
        foreach (Transform child in godPrayerPanel.transform)
        {
            if (child.name.Equals(godName))
            {
                child.gameObject.SetActive(true);               
                if (child.name.Contains("Shiva"))
                {
                    prayerAudioScript.AssignPrayer("Audio/Weekly/Aarti_Monday");
                }
                else if (child.name.Contains("Hanuman"))
                {
                    prayerAudioScript.AssignPrayer("Audio/Weekly/Aarti_Tuesday");
                }
                else {
                    prayerAudioScript.AssignPrayer("Audio/God/"+child.name);
                }

            }
            else {
                child.gameObject.SetActive(false);
            }
        }
    }
}
