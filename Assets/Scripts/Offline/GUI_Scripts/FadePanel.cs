﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FadePanel : MonoBehaviour {

    private Image panelImage;

	// Use this for initialization
	void Awake () {
        panelImage = this.GetComponent<Image>();
    }

    internal void BlackFade() {
        this.gameObject.SetActive(true);
        panelImage.CrossFadeAlpha(1, 0, true);
        panelImage.CrossFadeAlpha(0, 1, true);
        Invoke("SelfDeactivate",1);
    }

    void SelfDeactivate() {
        this.gameObject.SetActive(false);
    }
}

