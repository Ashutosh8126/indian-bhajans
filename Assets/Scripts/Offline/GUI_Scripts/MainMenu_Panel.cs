﻿using UnityEngine;
using System.Collections;

public class MainMenu_Panel : MonoBehaviour {

#region Public_Variables

#endregion

#region Private_Variables
	private ArtiMenu artiMenuScript;
    private FadePanel fadePanelScript;
#endregion

	// Use this for initialization
	void Awake () {
		artiMenuScript = this.transform.parent.transform.Find ("ArtiMenu").GetComponent<ArtiMenu> ();
        fadePanelScript = this.transform.parent.transform.Find("FadePanel").GetComponent<FadePanel>();

    }

	void Update(){
		if(Input.GetKeyDown(KeyCode.Escape)){
			Application.Quit ();
		}
	}


	public void MainMenuButtonAction(string buttonName){
		Debug.Log ("Button Pressed "+buttonName);
        fadePanelScript.BlackFade();
        this.gameObject.SetActive (false);
		artiMenuScript.SetArti_Menu (buttonName);
	}
}
