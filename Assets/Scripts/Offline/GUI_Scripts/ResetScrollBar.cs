﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ResetScrollBar : MonoBehaviour {

#region Private_Variables
	private Scrollbar localScrollBar;
#endregion

	// Use this for initialization
	void Awake () {
		localScrollBar = this.GetComponent<Scrollbar>();
	}
	
	void OnEnable(){
		Invoke ("ResetBar",0.1f);
	}

	internal void ResetBar(){
		localScrollBar.value = 1;
		Debug.Log ("Resetting Scroll Bar");
	}
}
