﻿using UnityEngine;
using System.Collections;

public class RotateAroundElement : MonoBehaviour {

 #region Public_Variables
    public Transform rotateAround_Transform;
    public float rotateSpeed = 0.5f;
    #endregion

    #region Private_Variables
    private Quaternion localRotation;
#endregion

    // Use this for initialization
    void Awake () {
        localRotation = this.transform.rotation;
    }
	
	// Update is called once per frame
	void Update () {
        this.transform.RotateAround(rotateAround_Transform.position, Vector3.forward, rotateSpeed * Time.deltaTime);
        this.transform.rotation = localRotation;
	}
}
