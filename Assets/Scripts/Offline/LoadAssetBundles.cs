﻿using UnityEngine;
using System.Collections;

public class LoadAssetBundles : MonoBehaviour {

#region Public_Variables
	public string BundleURL;
	public string AssetName;
	public int version;
#endregion

#region Private_Variables

#endregion

	// Use this for initialization
	void Awake () {
		StartCoroutine ("DownloadAndCache");
	}

	internal void LoadBundleAndCache(string bundleName){
		
	}
	
	private IEnumerator DownloadAndCache(){
		while(Caching.ready){
			yield return null;

			// Load the AssetBundle file from Cache if it exists with the same version or download and store it in the cache
			using(WWW www = WWW.LoadFromCacheOrDownload (BundleURL, version)){
				Debug.Log ("Loading Asset Bundle from Server");
				Debug.Log ("Progess "+www.progress.ToString());
				while (!www.isDone) {
					Debug.Log ("Progess "+www.progress.ToString());
					yield return null;
				}			
				if (www.error != null) {
					Debug.LogError("WWW download had an error:" + www.error);
				}
				{
					AssetBundle bundle = www.assetBundle;
					Debug.Log ("Bundle Name " + www.assetBundle.ToString ());
					if (AssetName == "god_artis")
						Instantiate(bundle.mainAsset);
					else
						Instantiate(bundle.LoadAsset(AssetName));
					// Unload the AssetBundles compressed contents to conserve memory
					bundle.Unload(false);
				}
			} // memory is freed from the web stream (www.Dispose() gets called implicitly)
		}
	}
}
