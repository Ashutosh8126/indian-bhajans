﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PrayerAudio : MonoBehaviour {

    public Text debugText;
    public Slider audioSlider;
    public Image playButton;
	private AudioSource myAudioSource;
	private AudioClip audioClipAssigned;
    private float audioDuration;

    private bool pauseAudio = false;

    // Use this for initialization
    void Awake()
    {
        myAudioSource = this.GetComponent<AudioSource>();
    }

	public void AssignPrayer(string audioName){
		myAudioSource.clip = Resources.Load<AudioClip> (audioName);
        myAudioSource.time = 0;
        audioSlider.value = 0;
        ResetAudio();
        pauseAudio = true;
        playButton.overrideSprite = Resources.Load<Sprite>("Images/Play_Btn");
        //audioClipAssigned =
        //	myAudioSource.Play ();
        //  myAudioSource
    }

    void Update() {
        if (myAudioSource.isPlaying) {
            // Debug.Log("Audio Source Progress "+myAudioSource.time);
           // debugText.text = "Current Audio Time " + myAudioSource.time;
            audioSlider.value = myAudioSource.time / audioDuration;
        }   
    }

	public void PlayAudio(){
        pauseAudio = !pauseAudio;
        if(!pauseAudio){
            Debug.Log("Pause Sprite");
            playButton.overrideSprite = Resources.Load<Sprite>("Images/Pause_Btn");
            myAudioSource.Play();
        }
        else {
            Debug.Log("Play Sprite");
            playButton.overrideSprite = Resources.Load<Sprite>("Images/Play_Btn");
            PauseAudio();
        }
	}

    public void PauseAudio() {        
        myAudioSource.Pause();
    }

	public void StopPrayer(){
		myAudioSource.Stop ();
	}

    public void ResetAudio() {
        pauseAudio = false;
    }

    public void UpdateAudioProgress() {
       // Debug.Log("Updating Audio Fucntion Called");
        audioDuration = myAudioSource.clip.length;
        float newPos = (audioSlider.value * audioDuration );
        myAudioSource.time = newPos;
      //  Debug.Log("Updating Audio to "+newPos);
     
    }

    public void TestSlider() {
       // Debug.Log("Test Slider");
    }
}
